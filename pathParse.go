package goSVGParse

import (
	"gitlab.com/beardio/goSVG"
	"gitlab.com/beardio/transform"
	"regexp"
	"runtime"
	"sync"
)

func parsePoints(command string, trans string, donePolygons chan<- Polygons, errChan chan error) {
	var x, y, w, h float64
	var err error
	var points Polygons
	re := regexp.MustCompile("[0-9]+,[0-9]+")
	D := re.FindAllString(command, -1)
	for _, d := range D {
		tx, ty, err := pathConv(d)
		if err != nil {
			errChan <- err
		}
		points.Pts = append(points.Pts, Points{X: tx, Y: ty})
	}

	if trans != "" {
		tw, th := maxXY(points.Pts)
		tx, ty := minXY(points.Pts)
		x, y, w, h, err = transform.ApplyTransform(tx, ty, tw, th, trans)
		if err != nil {
			errChan <- err
		}
	}
	tmpPts := Polygons{}
	tmpPts.Pts = append(tmpPts.Pts, Points{X: x, Y: y})
	tmpPts.Pts = append(tmpPts.Pts, Points{X: w, Y: h})
	donePolygons <- points
	donePolygons <- tmpPts
}

func parsePath(p chan goSVG.SvgPath, donePolygons chan<- Polygons, errChan chan error, wg *sync.WaitGroup) {
	//get the (minX,minY), (minX,maxY), (maxX,minY), (maxX, maxY)
	//and thats what transformed
	defer wg.Done()
	for v := range p {
		parsePoints(v.D, v.Transform, donePolygons, errChan)
	}
}

func maxXY(points []Points) (maxX, maxY float64) {
	for _, v := range points {
		if v.X > maxX {
			maxX = v.X
		}
		if v.Y > maxY {
			maxY = v.Y
		}
	}
	return
}

func minXY(points []Points) (minX, minY float64) {
	minX = points[0].X
	minY = points[0].Y
	for _, v := range points {
		if v.X < minX {
			minX = v.X
		}
		if v.Y < minY {
			minY = v.Y
		}
	}
	return
}

func receivePath(svg []goSVG.SvgPath) (p chan goSVG.SvgPath) {
	p = make(chan goSVG.SvgPath)
	go func() {
		defer close(p)
		for _, v := range svg {
			p <- v
		}
	}()
	return
}

func sendPath(p chan goSVG.SvgPath, errChan chan error) (donePolygons chan Polygons) {
	donePolygons = make(chan Polygons)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go parsePath(p, donePolygons, errChan, &wg)
	}

	go func() {
		wg.Wait()
		close(errChan)
		close(donePolygons)
	}()
	return
}
