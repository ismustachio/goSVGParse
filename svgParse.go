package goSVGParse

import (
	"gitlab.com/beardio/goSVG"
)

type Points struct {
	X float64
	Y float64
}

type Polygons struct {
	Pts []Points
}

func merge(elementChan ...<-chan Polygons) (out []Polygons) {
	out = make([]Polygons, 0)
	for _, c := range elementChan {
		go func(c <-chan Polygons) {
			out = append(out, <-c)
		}(c)
	}
	return
}

func ParseSVG(svg goSVG.Svg) ([]Polygons, error) {
	rChan := receiveRectangles(svg.Rectangles)
	eChan := receiveEllipses(svg.Ellipses)
	cChan := receiveCircles(svg.Circles)
	lChan := receiveLines(svg.Line)
	pthChan := receivePath(svg.Path)
	polyLineChan := receivePoly(svg.Polyline)
	polygonChan := receivePoly(svg.Polygon)
	errChan := make(chan error, 1)
	p := merge(sendRectangles(rChan, errChan),
		sendEllipses(eChan, errChan),
		sendCircles(cChan, errChan),
		sendLines(lChan, errChan),
		sendPath(pthChan, errChan),
		sendPoly(polyLineChan, errChan),
		sendPoly(polygonChan, errChan))

	err := <-errChan
	if err != nil {
		return nil, err
	}
	return p, nil
}
