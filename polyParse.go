package goSVGParse

import (
	"gitlab.com/beardio/goSVG"
	"runtime"
	"sync"
)

func receivePoly(svg []goSVG.SvgPolygon) (ply chan goSVG.SvgPolygon) {
	ply = make(chan goSVG.SvgPolygon)
	go func() {
		defer close(ply)
		for _, v := range svg {
			ply <- v
		}
	}()
	return
}

func sendPoly(ply chan goSVG.SvgPolygon, errChan chan error) (donePolygons chan Polygons) {
	donePolygons = make(chan Polygons)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go parsePoly(ply, donePolygons, errChan, &wg)
	}

	go func() {
		wg.Wait()
		close(errChan)
		close(donePolygons)
	}()
	return
}

func parsePoly(ply chan goSVG.SvgPolygon, donePolygons chan Polygons, errChan chan error, wg *sync.WaitGroup) {
	defer wg.Done()
	for p := range ply {
		parsePoints(p.Points, p.Transform, donePolygons, errChan)
	}
}
