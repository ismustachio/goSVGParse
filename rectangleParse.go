package goSVGParse

import (
	"gitlab.com/beardio/goSVG"
	"gitlab.com/beardio/transform"
	"runtime"
	"sync"
)

func receiveRectangles(svg []goSVG.SvgRect) (r chan goSVG.SvgRect) {
	r = make(chan goSVG.SvgRect)
	go func() {
		defer close(r)
		for _, v := range svg {
			r <- v
		}
	}()
	return
}

func sendRectangles(r chan goSVG.SvgRect, errChan chan error) (donePolygons chan Polygons) {
	donePolygons = make(chan Polygons)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go parseRectangle(r, donePolygons, errChan, &wg)
	}

	go func() {
		wg.Wait()
		close(errChan)
		close(donePolygons)
	}()
	return
}

func parseRectangle(rect chan goSVG.SvgRect, donePolygons chan<- Polygons, errChan chan error, wg *sync.WaitGroup) {
	defer wg.Done()
	for r := range rect {
		x, y, w, h, err := conv(r.X, r.Y, r.Width, r.Height)
		if err != nil {
			errChan <- err
		}
		if r.Transform != "" {
			x, y, w, h, err = transform.ApplyTransform(x, y, w, h, r.Transform)
			if err != nil {
				errChan <- err
			}
		}
		p := Polygons{}
		p.Pts = append(p.Pts, Points{X: x, Y: y})
		p.Pts = append(p.Pts, Points{X: x + w, Y: y})
		p.Pts = append(p.Pts, Points{X: x, Y: y + h})
		p.Pts = append(p.Pts, Points{X: x + w, Y: y + h})
		donePolygons <- p
	}
}
