package goSVGParse

import (
	"gitlab.com/beardio/goSVG"
	"gitlab.com/beardio/transform"
	"math"
	"runtime"
	"sync"
)

func parseEllipses(elli chan goSVG.SvgEllipses, donePolygons chan<- Polygons, errChan chan error, wg *sync.WaitGroup) {
	defer wg.Done()
	for e := range elli {
		cx, cy, w, h, err := conv(e.Cy, e.Cx, e.Rx, e.Ry)
		if err != nil {
			errChan <- err
		}
		if e.Transform != "" {
			cx, cy, w, h, err = transform.ApplyTransform(cx, cy, w, h, e.Transform)
			if err != nil {
				errChan <- err
			}
		}
		p := Polygons{}
		mRadius := math.Max(w, h)
		num := math.Ceil((2 * math.Pi) / math.Acos(1-(2/mRadius)))
		if num < 3 {
			num = 3
		}

		for i := float64(0); i < num; i++ {
			theta := i * ((2 * math.Pi) / num)
			p.Pts = append(p.Pts, Points{X: (r*math.Cos(theta) + cx), Y: mRadius*math.Sin(theta) + cy})
		}
		donePolygons <- p

	}
}

func receiveEllipses(svg []goSVG.SvgEllipses) (elli chan goSVG.SvgEllipses) {
	elli = make(chan goSVG.SvgEllipses)
	go func() {
		defer close(elli)
		for _, v := range svg {
			elli <- v
		}
	}()
	return
}

func sendEllipses(e chan goSVG.SvgEllipses, errChan chan error) (donePolygons chan Polygons) {
	donePolygons = make(chan Polygons)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go parseEllipses(e, donePolygons, errChan, &wg)
	}

	go func() {
		wg.Wait()
		close(errChan)
		close(donePolygons)
	}()
	return
}
