package goSVGParse

import (
	"gitlab.com/beardio/goSVG"
	"gitlab.com/beardio/transform"
	"math"
	"runtime"
	"sync"
)

func parseCircle(cir chan goSVG.SvgCircle, donePolygons chan Polygons, errChan chan error, wg *sync.WaitGroup) {
	defer wg.Done()
	for c := range cir {
		cx, cy, r, err := circleConv(c.Cx, c.Cy, c.R)
		if err != nil {
			errChan <- err
		}
		if c.Transform != "" {
			cx, cy, r, _, err = transform.ApplyTransform(cx, cy, r, r, c.Transform)
			if err != nil {
				errChan <- err
			}
		}
		p := Polygons{}
		num := math.Ceil((2 * math.Pi) / math.Acos(1-(2/r)))
		if num < 3 {
			num = 3
		}

		for i := float64(0); i < num; i++ {
			theta := i * ((2 * math.Pi) / num)
			p.Pts = append(p.Pts, Points{X: r*math.Cos(theta) + cx, Y: r*math.Sin(theta) + cy})
		}
		donePolygons <- p
	}
}

func receiveCircles(svg []goSVG.SvgCircle) (c chan goSVG.SvgCircle) {
	c = make(chan goSVG.SvgCircle)
	go func() {
		defer close(c)
		for _, v := range svg {
			c <- v
		}
	}()
	return
}

func sendCircles(c chan goSVG.SvgCircle, errChan chan error) (donePolygons chan Polygons) {
	donePolygons = make(chan Polygons)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go parseCircle(c, donePolygons, errChan, &wg)
	}

	go func() {
		wg.Wait()
		close(errChan)
		close(donePolygons)
	}()
	return
}
