package goSVGParse

import (
	"fmt"
	"gitlab.com/beardio/goSVG"
	"log"
	"testing"
)

func TestRectangleParse(t *testing.T) {
	gsvg := &goSVG.Svg{}
	err := gsvg.UnMarshal(testFile)
	if err != nil {
		log.Fatal(err)
	}
	r := receiveRectangles(gsvg.Rectangles)
	errChan := make(chan error, 1)
	for v := range sendRectangles(r, errChan) {
		fmt.Println(v)
	}
	err = <-errChan
	if err != nil {
		log.Fatal(fmt.Errorf("%v\n", err))
	}
}

func TestTransformation(t *testing.T) {

}

var testFile = "testdata/all.svg"

func TestParseEllipses(t *testing.T) {
	gsvg := &goSVG.Svg{}
	err := gsvg.UnMarshal(testFile)
	if err != nil {
		log.Fatal(err)
	}
	r := receiveEllipses(gsvg.Ellipses)
	errChan := make(chan error, 1)
	for v := range sendEllipses(r, errChan) {
		fmt.Println(v)
	}
	err = <-errChan
	if err != nil {
		log.Fatal(fmt.Errorf("%v\n", err))
	}
}

func TestParseCircle(t *testing.T) {
	gsvg := &goSVG.Svg{}
	err := gsvg.UnMarshal(testFile)
	if err != nil {
		log.Fatal(err)
	}
	c := receiveCircles(gsvg.Circles)
	errChan := make(chan error, 1)
	for v := range sendCircles(c, errChan) {
		fmt.Println(v)
	}
	err = <-errChan
	if err != nil {
		log.Fatal(fmt.Errorf("%v\n", err))
	}
}

func TestParseLine(t *testing.T) {
	gsvg := &goSVG.Svg{}
	err := gsvg.UnMarshal(testFile)
	if err != nil {
		log.Fatal(err)
	}
	l := receiveLines(gsvg.Line)
	errChan := make(chan error, 1)
	for v := range sendLines(l, errChan) {
		for _, k := range v.Pts {
			if k.X == -1 {

			}
		}
	}
	err = <-errChan
	if err != nil {
		log.Fatal(fmt.Errorf("%v\n", err))
	}

}

func TestParsePoly(t *testing.T) {
	gsvg := &goSVG.Svg{}
	err := gsvg.UnMarshal(testFile)
	if err != nil {
		log.Fatal(err)
	}
	poly := receivePoly(gsvg.Polygon)
	errChan := make(chan error, 1)
	for v := range sendPoly(poly, errChan) {
		if v.Pts == nil {
			return
		}
	}
}

func TestParsePath(t *testing.T) {
	gsvg := &goSVG.Svg{}
	err := gsvg.UnMarshal(testFile)
	if err != nil {
		log.Fatal(err)
	}
	p := receivePath(gsvg.Path)
	errChan := make(chan error, 1)
	for v := range sendPath(p, errChan) {
		if v.Pts == nil {
			return
		}
	}
	err = <-errChan
	if err != nil {
		log.Fatal(fmt.Errorf("%v\n", err))
	}
}

func TestParseSVG(t *testing.T) {
	gsvg := &goSVG.Svg{}
	err := gsvg.UnMarshal(testFile)
	if err != nil {
		log.Fatal(err)
	}
	p, err := ParseSVG(*gsvg)
	//for _, v := range p {
	//	fmt.Println(v)
	//}
	if len(p) <= 0 {
		log.Fatal(fmt.Errorf("No polygons were returned.\n"))
	}
}
