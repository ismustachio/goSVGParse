package goSVGParse

import (
	"regexp"
	"strconv"
	"strings"
)

func circleConv(s1, s2, s3 string) (x, y, r float64, err error) {
	x, err = strconv.ParseFloat(s1, 64)
	if err != nil {
		return 0, 0, 0, nil
	}
	y, err = strconv.ParseFloat(s2, 64)
	if err != nil {
		return 0, 0, 0, nil
	}
	r, err = strconv.ParseFloat(s3, 64)
	if err != nil {
		return 0, 0, 0, nil
	}
	return
}

func conv(xCoord, yCoord, width, height string) (x float64, y float64, w float64, h float64, err error) {
	x, err = strconv.ParseFloat(xCoord, 64)
	if err != nil {
		return 0, 0, 0, 0, nil
	}
	y, err = strconv.ParseFloat(yCoord, 64)
	if err != nil {
		return 0, 0, 0, 0, nil
	}
	w, err = strconv.ParseFloat(width, 64)
	if err != nil {
		return 0, 0, 0, 0, nil
	}
	h, err = strconv.ParseFloat(height, 64)
	if err != nil {
		return 0, 0, 0, 0, nil
	}
	return
}

func pathConv(pts string) (x, y float64, err error) {
	reX := regexp.MustCompile("[0-9]+,")
	reY := regexp.MustCompile(",[0-9]+")
	sX := reX.FindAllString(pts, -1)
	sY := reY.FindAllString(pts, -1)
	parsedX := strings.Trim(strings.Join(sX, ""), ",")
	parsedY := strings.Trim(strings.Join(sY, ""), ",")
	x, err = strconv.ParseFloat(parsedX, 64)
	if err != nil {
		return 0, 0, err
	}
	y, err = strconv.ParseFloat(parsedY, 64)
	if err != nil {
		return 0, 0, err
	}
	return
}
