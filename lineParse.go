package goSVGParse

import (
	"gitlab.com/beardio/goSVG"
	"gitlab.com/beardio/transform"
	"runtime"
	"sync"
)

func sendLines(l chan goSVG.SvgLine, errChan chan error) (donePolygons chan Polygons) {
	donePolygons = make(chan Polygons)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go parseLines(l, donePolygons, errChan, &wg)
	}

	go func() {
		wg.Wait()
		close(errChan)
		close(donePolygons)
	}()
	return
}

func receiveLines(svg []goSVG.SvgLine) (l chan goSVG.SvgLine) {
	l = make(chan goSVG.SvgLine)
	go func() {
		defer close(l)
		for _, v := range svg {
			l <- v
		}
	}()
	return
}

func parseLines(li chan goSVG.SvgLine, donePolygons chan Polygons, errC chan error, wg *sync.WaitGroup) {
	defer wg.Done()
	for l := range li {
		x, y, w, h, err := conv(l.X1, l.Y1, l.X2, l.Y2)
		if err != nil {
			errC <- err
		}
		if l.Transform != "" {
			x, y, w, h, err = transform.ApplyTransform(x, y, w, h, l.Transform)
			if err != nil {
				errC <- err
			}
		}
		p := Polygons{}
		p.Pts = append(p.Pts, Points{X: x, Y: y})
		p.Pts = append(p.Pts, Points{X: x + w, Y: y})
		p.Pts = append(p.Pts, Points{X: x, Y: y + h})
		p.Pts = append(p.Pts, Points{X: x + w, Y: y + h})
		donePolygons <- p

	}
}
